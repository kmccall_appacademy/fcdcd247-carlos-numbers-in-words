class Fixnum
  def self.three_digit_words(digits)
    ones_place = {
      "1" => "one",
      "2" => "two",
      "3" => "three",
      "4" => "four",
      "5" => "five",
      "6" => "six",
      "7" => "seven",
      "8" => "eight",
      "9" => "nine"
    }
    tenplus_ones_place = {
      "0" => "ten",
      "1" => "eleven",
      "2" => "twelve",
      "3" => "thirteen",
      "4" => "fourteen",
      "5" => "fifteen",
      "6" => "sixteen",
      "7" => "seventeen",
      "8" => "eighteen",
      "9" => "nineteen"
    }
    tens_place = {
      "2" => "twenty",
      "3" => "thirty",
      "4" => "forty",
      "5" => "fifty",
      "6" => "sixty",
      "7" => "seventy",
      "8" => "eighty",
      "9" => "ninety"
    }
    result = ""
    if digits[0] != "0"
      result += ones_place[digits[0]] + " hundred "
    end
    if digits[1] == "1"
      result += tenplus_ones_place[digits[2]] + " "
    else
      if digits[1] != "0"
        result += tens_place[digits[1]] + " "
      end
      if digits[2] != "0"
        result += ones_place[digits[2]] + " "
      end
    end
    result[0..-2]
  end
  def in_words
    return "zero" if self == 0
    three_shift = {
      1 => "thousand",
      2 => "million",
      3 => "billion",
      4 => "trillion"
    }
    digits = self.to_s
    three_digit = []
    until digits.empty?
      if digits.length < 3
        three_digit << digits
        digits = ""
      else
        three_digit << digits[-3..-1]
        digits = digits[0...-3]
      end
    end
    result = ""
    (0...three_digit.length).to_a.reverse_each do |i|
      this_digits = ("0" * (3 - three_digit[i].length)) + three_digit[i]
      if this_digits == "000"
        next
      end
      result += Fixnum.three_digit_words(this_digits) + " "
      if i != 0
        result += three_shift[i] + " "
      end
    end
    result[0..-2]
  end
end
